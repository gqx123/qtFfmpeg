﻿#ifndef VIDEOITEM_H
#define VIDEOITEM_H

//#include <QQuickPaintedItem>
#include <QtQuick/QQuickPaintedItem>
#include<QImage>

class VideoItem : public QQuickPaintedItem
{
    Q_OBJECT
public:
    VideoItem(QQuickItem  *parent = nullptr);
    void setImg(QImage &img);

    virtual void paint(QPainter *painter) override;
signals:

public slots:

private:
    QImage m_img;
};

#endif // VIDEOITEM_H
