﻿#ifndef AUDIODECODE_H
#define AUDIODECODE_H

#include <QObject>
#include<QAudioOutput>
#include<QIODevice>
#include<QMutex>
#include<QMutexLocker>

class AudioDecode : public QObject
{
    Q_OBJECT
public:
    explicit AudioDecode(QObject *parent = nullptr);
    virtual ~AudioDecode();

    void Stop();
    bool Start();
    void Play(bool isPlay);
    bool Write(const char *data, int datasize);
    int GetFree();

    void setSample(int sampleRate,int sampleSize,int channel);

    int sampleRate = 32000;
    int sampleSize = 16;
    int channel = 2;

public:
    QAudioOutput *output = nullptr;
    QIODevice *io = nullptr;
    QMutex mutex;

signals:

public slots:
    void s_audio(QByteArray bt);
};

#endif // AUDIODECODE_H
