﻿#ifndef VIDEODECODE_H
#define VIDEODECODE_H

#include <QObject>
#include <QThread>
#include    <QList>
extern "C"{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
#include "libswscale/swscale.h"
};

class FFmpegOpt;
class ReadPaket : public QObject
{
    Q_OBJECT
public:
    explicit ReadPaket(QObject *parent = nullptr);
    virtual ~ReadPaket();
    void startRead(FFmpegOpt * ff);
signals:
    void sig_startThread();

public slots:
    void s_startThread();
private:
    FFmpegOpt * m_ffmegp = nullptr;
};

class VideoDecode:public QObject{
    Q_OBJECT
public:
    explicit VideoDecode(QObject *parent = nullptr);
    virtual ~VideoDecode(){}

    void startVideo(FFmpegOpt* ffm);
signals:
    void sig_startThread();
public slots:
    void s_startThread();
private:
    FFmpegOpt * m_ffmegp = nullptr;
};

class AudioDec:public QObject{
    Q_OBJECT
public:
    explicit AudioDec(QObject *parent = nullptr);
    virtual ~AudioDec(){}

    void startAudio(FFmpegOpt* ffm);
signals:
    void sig_startThread();
public slots:
    void s_startThread();
private:
    FFmpegOpt * m_ffmegp = nullptr;
};

#endif // VIDEODECODE_H
