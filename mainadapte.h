﻿#ifndef MAINADAPTE_H
#define MAINADAPTE_H

#include <QObject>
#include <QVariant>
#include <QMap>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTimer>
#include "ffmpegopt.h"

class MainAdapte : public QObject
{
    Q_OBJECT
public:
    explicit MainAdapte(QObject *parent = nullptr);

    void startExe();
    Q_INVOKABLE QVariantMap callCppFunc(QString funcStr,QVariantMap pData);
signals:
    void sig_callQmlFunc(QString funcStr,QVariantMap pData);

public slots:
    bool eventFilter(QObject *watched, QEvent *event);
    void s_play(QImage);

private:
    QQmlApplicationEngine *m_engine = nullptr;
    FFmpegOpt *m_ffmpeg = nullptr;

    QWidget *m_widget = nullptr;
    QImage m_img;

};

#endif // MAINADAPTE_H
