QT += quick
QT += widgets
QT += multimedia

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DESTDIR = ../bin

HEADERS += *.h
SOURCES += *.cpp


#RESOURCES += $$files(Resources/*.qrc)
RESOURCES += \
      $$files(qml/*.qml)\
      $$files(qml/subControl/*.qml)\
      $$files(qml/*.js)\

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


LIBS += -L$$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/lib/ -lavcodec
LIBS += -L$$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/lib/ -lavdevice
LIBS += -L$$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/lib/ -lavfilter
LIBS += -L$$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/lib/ -lavformat
LIBS += -L$$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/lib/ -lavutil
LIBS += -L$$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/lib/ -lpostproc
LIBS += -L$$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/lib/ -lswresample
LIBS += -L$$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/lib/ -lswscale


INCLUDEPATH += $$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/include
DEPENDPATH += $$PWD/thirdlib/ffmpeg-4.2.2-win32-dev/include



