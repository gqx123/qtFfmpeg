﻿#pragma once

#include <QObject>
#include <QtDebug>
#include <QThread>
#include<QTextEdit>

#define  qDLog() qDebug().noquote()<<QThread::currentThreadId()<<"-"<<__FUNCTION__<<"}"
#define  qILog() qInfo().noquote()<<QThread::currentThreadId()<<"-"<<__FUNCTION__<<"}"
#define  qWLog() qWarning().noquote()<<QThread::currentThreadId()<<"-"<<__FUNCTION__<<"}"
#define  qELog() qCritical().noquote()<<QThread::currentThreadId()<<"-"<<__FUNCTION__<<"}"


class QFile;

class QtLog : public QObject
{
	Q_OBJECT

public:
    enum LOG_LEVEL{L_DEBUG,L_INFO,L_WARNING,L_ERROR};
	~QtLog();

    QTextEdit* txtEdit = nullptr;
public:
	static void myMessageOutput(QtMsgType type, const QMessageLogContext& context, const QString& msg);
	static void writeData2File(QFile* file,QString& str);
    static QtLog& init(LOG_LEVEL);

private:
	QtLog(QObject* parent=nullptr);
private:
	static QString m_fileName;
	static QFile* m_Dfile;
    static QFile* m_Efile;
    int m_name;
    static LOG_LEVEL m_logLevel;

    static QString m_txt;

};
