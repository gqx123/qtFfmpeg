﻿#ifndef NETSTREAMVIDEO_H
#define NETSTREAMVIDEO_H

#include <QObject>
#include <QThread>
#include <QImage>
#include"audiodecode.h"


extern "C"{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
#include "libswscale/swscale.h"
};

class NetStreamVideo : public QObject
{
    Q_OBJECT
public:
    explicit NetStreamVideo(QObject *parent = nullptr);
    virtual ~NetStreamVideo();

    void openUrlVideo(QString url);
    void playVideo(bool b ){m_play = b;}

    bool isPlayVideo(){return  m_play;}

    QString getFFmpegError(int ret);
//    void decodeAudio(const AVPacket *pkt);
    int ToPCM(char *out,AVFormatContext *m_afc,AVFrame *avframe);
    bool YuvToRGB(AVFormatContext *m_afc,AVFrame *avframe, int outweight, int outheight);

signals:
    void sig_startThread();
    void sig_imageFrame(QImage img);
    void sig_audio(QByteArray b);
public slots:
    void s_startThread();

private:
    QThread *m_thread = nullptr;
    QString m_urlVideo = "";
    bool m_play = false;

    AudioDecode *m_audio = nullptr;
    int m_audioStream = -1;
};

#endif // NETSTREAMVIDEO_H
