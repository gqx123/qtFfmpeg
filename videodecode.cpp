﻿#include "videodecode.h"

#include "ffmpegopt.h"
#include "qtlog.h"
#include <QDateTime>

ReadPaket::ReadPaket(QObject *parent) : QObject(parent)
{

    static QThread *thread = new QThread();
    this->moveToThread(thread);
    thread->start();

    connect(this,&ReadPaket::sig_startThread,this,&ReadPaket::s_startThread);
}

ReadPaket::~ReadPaket()
{
    m_ffmegp->stopPlay();
    QThread *thread= this->thread();
    thread->wait(400);
    thread->exit();
    delete thread;
}

void ReadPaket::startRead( FFmpegOpt * ff)
{
    if(!ff)
        return;
    m_ffmegp = ff;
    emit sig_startThread();
}

void ReadPaket::s_startThread()
{
    while (1) {
        if(!m_ffmegp->isPlay()){
            QThread::msleep(5);
            continue;
        }
        int pSize = m_ffmegp->ReadFrame();
        if(pSize == -2){
            QThread::msleep(15);
            continue;
        }
        if(pSize <= 0 ){
            qDLog()<<"end";
            return;
        }
    }
}

VideoDecode::VideoDecode(QObject *parent)
{
    QThread *thread = new QThread();
    this->moveToThread(thread);
    thread->start();
    connect(this,&VideoDecode::sig_startThread,this,&VideoDecode::s_startThread);
}

void VideoDecode::startVideo(FFmpegOpt *ff)
{
    if(!ff)
        return;
    m_ffmegp = ff;
    emit sig_startThread();
}

void VideoDecode::s_startThread()
{
    while (1) {
        if(!m_ffmegp->isPlay()){
            QThread::msleep(5);
            continue;
        }
        if(!m_ffmegp->decodeVideo())
            QThread::msleep(5);
    }
}


AudioDec::AudioDec(QObject *parent)
{
    QThread *thread = new QThread();
    this->moveToThread(thread);
    thread->start();
    connect(this,&AudioDec::sig_startThread,this,&AudioDec::s_startThread);
}


void AudioDec::startAudio(FFmpegOpt *ff)
{
    if(!ff)
        return;
    m_ffmegp = ff;
    emit sig_startThread();
}

void AudioDec::s_startThread()
{
    while (1) {
        if(!m_ffmegp->isPlay()){
            QThread::msleep(5);
            continue;
        }
        if(!m_ffmegp->decodeAudio())
            QThread::msleep(5);
    }
}
