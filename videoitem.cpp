﻿#include "videoitem.h"


#include<QPainter>

VideoItem::VideoItem(QQuickItem  *parent)
    : QQuickPaintedItem(parent)
{

}

void VideoItem::setImg(QImage &img)
{
    m_img = img;
    update();
}

void VideoItem::paint(QPainter *painter)
{
    painter->setRenderHint(QPainter::Antialiasing);

    if(!m_img.isNull()){
       painter->drawImage(QRect(0,0,size().width(),size().height()),m_img);
    }
}
