﻿#include <QGuiApplication>
#include<QApplication>
#include "mainadapte.h"
#include"qtlog.h"

int main(int argc, char *argv[])
{
//    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    QtLog::init(QtLog::LOG_LEVEL::L_DEBUG);                      //初始日志库
    MainAdapte a;
    a.startExe();
    return app.exec();
}
