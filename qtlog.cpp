﻿#include "qtlog.h"
#include <QDebug>
#include <QDateTime>
#include <QFile>
#include <QSettings>
#include<QTextEdit>
#include<QTimer>

#define LOG_Dir  "./log/"

QtLog::LOG_LEVEL QtLog::m_logLevel = L_DEBUG;
QString QtLog::m_fileName = QDateTime::currentDateTime().toString("yyyyMMdd.hhmmsszzz");

QString QtLog::m_txt = "";

QFile* QtLog::m_Dfile = nullptr;
QFile* QtLog::m_Efile = nullptr;

QtLog::QtLog(QObject *parent)
	: QObject(parent)
{
	qInstallMessageHandler(myMessageOutput);


    txtEdit = new QTextEdit(nullptr);
    txtEdit->show();
    QTimer *tim = new QTimer(this);

    connect(tim,&QTimer::timeout,this,[=]{
        txtEdit->append(m_txt);
        m_txt.clear();
    });

    tim->start(1000*2);

}

QtLog::~QtLog()
{
	if (m_Dfile) {
        m_Dfile->flush();
		m_Dfile->close();
		delete m_Dfile;
	}
		
	if (m_Efile){
        m_Efile->flush();
		m_Efile->close();
		delete m_Efile;
	}
}

QtLog& QtLog::init(LOG_LEVEL lev)
{
    //QSettings settings("config",QSettings::IniFormat);
    m_logLevel = lev;//(LOG_LEVEL)settings.value("logLevel",0).toInt();

	static QtLog log(nullptr);
	return log;
}

void QtLog::myMessageOutput(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{    
//    return;
    QString strType = "Debug";
    switch (type) {
    case QtDebugMsg:
		strType = "D";
		break;
	case QtInfoMsg:
		strType = "I";
		break;
	case QtWarningMsg:
		strType = "W";
		break;
	case QtCriticalMsg:
		strType = "E";
		break;
	case QtFatalMsg:
		strType = "F";
        abort();
	}
    QList<QtMsgType> listType = {QtDebugMsg,QtInfoMsg,QtWarningMsg,QtCriticalMsg};
    int lv = listType.indexOf(type);
    bool isWrite = (lv >= m_logLevel);
//    isWrite = !(type == QtWarningMsg);
//    if(!isWrite)
//        return;

    QString dateStr = QDateTime::currentDateTime().toString("yyyyMMdd.hhmmss.zzz");
	QString codeFile(context.file);
	if (context.line != 0) {
		codeFile = codeFile.mid(codeFile.lastIndexOf("\\")+1);
	}
	
    QString logStr = QString("%1:%2(%3:%4)%5\n").arg(strType).arg(dateStr).arg(codeFile).arg(context.line).arg(msg);
    QString logDir = LOG_Dir;
	if (type == QtCriticalMsg && m_Efile == nullptr) {
		m_Efile = new QFile(logDir + strType + m_fileName);
		m_Efile->open(QIODevice::WriteOnly);
	}

	if (m_Dfile == nullptr) {
		m_Dfile = new QFile(logDir + "I" + m_fileName);
		m_Dfile->open(QIODevice::WriteOnly);
	}	

//    if(isWrite){
//        if (type == QtCriticalMsg)
//        {
//            writeData2File(m_Efile, logStr);
//        }
//        writeData2File(m_Dfile, logStr);
//    }

#if 1	//调试用

    QTextEdit *txtedit = QtLog::init(m_logLevel).txtEdit;
    if(!txtedit)
        return;

    m_txt.append(logStr);
    if(m_txt.size() > 1024*10){
        txtedit->append(logStr);
        m_txt.clear();
    }else{
        return;
    }

    if (txtedit->isHidden())
        txtedit->show();
#endif // _DEBUG
}

void QtLog::writeData2File(QFile* file, QString& str)
{
	file->write(str.toLocal8Bit());
//    file->flush();
}
