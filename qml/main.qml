﻿import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5


import VideoItem 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    Connections{
        target: CPPObJ
        onSig_callQmlFunc:cppCallQmlJs(funcStr,pData)
    }

    function cppCallQmlJs(funcStr,pData){
        console.log(funcStr,JSON.stringify(pData))
    }

    Item {
        anchors.fill: parent;
        Rectangle{
            anchors.fill: parent
            color: "red"
        }

        VideoItem{
            anchors.fill: parent
            objectName: "videoItem"
        }

        Button{
            id:playBtn
            y:100
            text: qsTr("Play")
            onClicked: {
                var t ={}
                t = CPPObJ.callCppFunc("playVideo",t)
                timeSlid.to = t["total"]
                timeGetProgress.start()
            }
        }
        Button{
            id:pauseBtn
            y:100
            text: qsTr("Pause")
            anchors.left: playBtn.right
            anchors.leftMargin: 20
            onClicked: {
                var t ={}
                t = CPPObJ.callCppFunc("pauseVideo",t)
                timeGetProgress.stop()
            }
        }
        Button{
            id:stopBtn
            y:100
            anchors.left: pauseBtn.right
            anchors.leftMargin: 20
            text: qsTr("Stop")
            onClicked: {
                var t ={}
                t = CPPObJ.callCppFunc("stop",t)
                timeGetProgress.stop()
            }
        }
        Slider{
            id:timeSlid
            anchors.bottom: playBtn.top
            anchors.bottomMargin: 40
            width: parent.width
            from:1
            to:100
            value: 0
            onMoved: {
                var t={}
                t["pos"] = value/to
                t = CPPObJ.callCppFunc("seekToPos",t)
            }
        }

        Text {
            id: timeTxt
            anchors.horizontalCenter: parent.horizontalCenter
            color: "white"
            font.pointSize: 16
            text: qsTr("text")
        }

        Timer{
            id:timeGetProgress
            interval: 300
            repeat: true
            onTriggered: {
                var t ={}
                t = CPPObJ.callCppFunc("getTime",t)
                var pos = t["pos"]
                var total = t["total"];
                timeSlid.value = pos
                timeTxt.text = parseInt(pos/60) +":"+ pos%60 +"  "+ parseInt(total/60)+":"+total%60
            }
        }
        Row{
            anchors.bottom: parent.bottom
            width: parent.width
            TextField{
                id:fieldUrl
                selectByMouse:true
                text: "rtmp://58.200.131.2:1935/livetv/dftv"
                width: parent.width - openUrl.width
            }
            Button{
                id:openUrl
                text: qsTr("打开网络流")
                onClicked: {
                    var t ={}
                    t["url"] = fieldUrl.text
                    t = CPPObJ.callCppFunc("openUrlVideo",t)
                }
            }
        }


    }
}
