﻿#ifndef AUDIOENCODE_H
#define AUDIOENCODE_H

#include <QObject>
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#ifdef __cplusplus
};

class AudioEncode : public QObject
{
    Q_OBJECT
public:
    AudioEncode();
    ~AudioEncode(){}

    void startEncodeAudio();

    int flush_encoder(AVFormatContext *fmt_ctx,unsigned int stream_index);

signals:
public slots:

private:

};

#endif // AUDIOENCODE_H
