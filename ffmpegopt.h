﻿#ifndef FFMPEGOPT_H
#define FFMPEGOPT_H

#include <QObject>
#include <QMap>
#include <QMutex>
#include <QMutexLocker>
#include<QImage>
#include "videodecode.h"
#include "audiodecode.h"
#include "netstreamvideo.h"



class FFmpegOpt : public QObject
{
    Q_OBJECT
public:
    explicit FFmpegOpt(QObject *parent = nullptr);
    virtual ~FFmpegOpt();

    QString getAvConfig();                          // 获得编译参数
    QVariantMap openVideo(QString strPath);         //打开视频
    void openUrlVideo(QString url);                 //打开网络视频
    int ReadFrame();                                //读帧
    void DecodeFrame(const AVPacket *pkt);          //解码

    void pausePlay(bool b){m_isPlay = b;}
    bool isPlay();
    void stopPlay();

    void seekPos(double pos);
    int getPos(){return  m_pos;}
    int getTotal(){return m_total;}

    bool decodeVideo();
    bool decodeAudio();

signals:
    void sig_play(QImage);
    void sig_audio(QByteArray);

private:
//    void decodeVideo(const AVPacket *pkt);
//    void decodeAudio(const AVPacket *pkt);
    int  ToPCM(AVFrame *frame);
    bool YuvToRGB(AVFrame *frame, int outweight, int outheight);     //格式转换

    QString getFFmpegError(int ret);
    void clearFFmpeg();
public slots:
private:
    bool m_isPlay = false;

    AVFormatContext *m_afc = nullptr;
    AVFrame *m_yuv = nullptr;
    AVFrame *m_pcm = nullptr;
    SwsContext *m_cCtx = nullptr;
    void *m_aCtx = nullptr;


    int m_videoStream = -1;
    int m_audioStream = -1;
    VideoDecode *m_videoDecod = nullptr;
    AudioDec *m_audioDec = nullptr;
    ReadPaket *m_readPk = nullptr;
    QMutex m_mutex ;

    AudioDecode *m_audio = nullptr;

    double m_pos = 0;
    int m_total = 1;

    NetStreamVideo *m_netStream = nullptr;

    QList<AVPacket> m_gList;
};

#endif // FFMPEGOPT_H
